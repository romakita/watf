function include(fileName) {
	document.write("<script type='text/javascript' src='"+fileName+"' charset='utf-8'></script>" );
}
include("/js/jquery.min.js");
include("/js/idangerous.swiper-2.6.min.js");
include("/js/path.js");
include("/js/single.js");

var coverVideo = document.getElementById("cover_video");

document.addEventListener("DOMContentLoaded", function(event)
{
    document.removeEventListener(event.type, arguments.callee, false);
    
    // Init Swiper*
    var swiper = new Swiper('.swiper-container', {
        keyboardControl: true,
        simulateTouch: false,
        onSlideChangeEnd: Single.onSlideChangeEnd,
        onSlideTouch: Single.onSlideTouch
    });
    
    // Init Single*
    Single.init({ 
        log: false,
        lang: "fr",
        swiper: swiper,    
        idSepartator: "__",
        mainContainer: document.getElementById("main-container"),
        navContainer: document.getElementById("nav-container"),
        mediaContainer: document.getElementById("media-container"),
        onComplete: function(params) {
            console.debug("[WATF]* #02 - Digitalisation de l'espace urbain.");
        }
    });
    
    // fill the cover video    
    if(coverVideo.videoWidth > 0) {
        Single.fillVideo(coverVideo);
    }
    else {
        coverVideo.addEventListener("loadedmetadata", function(event) {        
            Single.fillVideo(coverVideo);        
        });    
    }
    
});

window.addEventListener("resize", function(event) {
    Single.fillVideo(coverVideo);
}); 


  
