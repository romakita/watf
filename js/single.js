/** ======================================================================
 * Single JS
 *
 * This Object manage lot of useful things for a Single Page Website.
 * Mobile, Share, Sections, Media player, etc.
 *
 * Author: founsy@icloud.com
 *
 ======================================================================**/

/**
 * The global Single object
 **/
var Single = {

    // variables
    log: false,
    rootPath: "/",
    lang: "en",
    swiper: null,
    idSeparator: "__",
    mainContainer: null,
    navContainer: null,
    mediaContainer: null,
    metaTags: {},
    sectionCount: 0,
    sectionIndexes: {},
    currentPath: this.rootPath,
    currentMetadata: {},
    currentMedia: null,
    currentMediaType: "media",
    onComplete: null,
    menuOpen: false,
    mediaOpen: false,
    isMobile: false,
    preventPushState: false,

    /*--------------------------------------
     Public functions API
     ---------------------------------------*/

    /**
     * Initialize Single object
     * @param params A big object with all params
     */
    init: function(params) {

        Single = this;

        // Get base URL
        var base = document.getElementsByTagName("base")[0];
        if(base) {
            this.rootPath = base.getAttribute("href");
        }
        else {
            this.rootPath = "/";
        }

        // Retreive params
        if(params != null) {
            this.log = params.log;
            this.lang = params.lang;
            this.swiper = params.swiper;
            this.mainContainer = params.mainContainer;
            this.navContainer = params.navContainer;
            this.mediaContainer = params.mediaContainer;
            this.onComplete = params.onComplete;
        }

        // Test if is mobile device
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            this.isMobile = true;
        }

        // Finally complete.
        this.singleComplete();
    },

    /**
     * Called when Single initilize is complete
     **/
    singleComplete: function() {

        // init Swipe
        if(this.swiper) {
            this._registerSections();
            //this.swiper.disableMousewheelControl();
        }

        // init Path
        if(Path)
            this._registerPath();

        // Meta tags / Facebook Open graph / Twitter
        this._registerMetaTags();

        // active links
        this._activeLinks();

        var params = document.location.href.replace(document.location.origin, '');

        if(params.replace(/\/$/, '') != this.rootPath.replace(/\/$/, '')){

            var path  = params.replace(this.rootPath, '');

            if(document.getElementById(path.replace('/', '__'))){

                if(this.log)
                    console.debug("Node find by path");

                this._slideByPath(path);
            }else{
                document.body.classList.remove("loading");
            }
        }

        document.body.classList.remove("loading");


        // loading state
        //document.body.classList.remove("loading");

        if(this.log)
            console.debug("Single Page Website is ready.");

        // callback complete
        if(this.onComplete)
            this.onComplete.call(this);

    },


    /**
     * Go to the next slide
     */
    gotoNext: function() {
        Single.swiper.swipeNext();
        if(Single.swiper.activeIndex >= Single.sectionCount)
            return false;
    },

    /**
     * Go to the previous slide
     */
    gotoPrevious: function() {
        this.swiper.swipePrev();
        if(this.swiper.activeIndex == 0)
            return false;
    },

    /**
     * Slide touch listener
     */
    onSlideTouch: function() {
        if(Single.menuOpen)
            Single.openCloseMenu();
    },

    /**
     * Slide change end listner
     */
    onSlideChangeEnd: function(swiper) {

        try {

            Single._slideChanged();

            // loading state
            document.body.classList.remove("loading");

            if (jQuery &&  /Safari/i.test(navigator.userAgent) && !/Mobile/i.test(navigator.userAgent)){
                console.log('FIX SAFARI MAC BUG');

                jQuery('.article-scroller').css('height', '99%').scrollTop(0);

                setTimeout(function () {
                    jQuery('.article-scroller').css('height', '100%').scrollTop(0);
                    console.log('FIXED SAFARI MAC BUG');
                }, 2);
            }

        }catch(er){
            alert(er);
        }
    },

    /**
     * Share the current page by the passed social network
     */
    shareBy: function(by, share_url) {
        var url;
        if(!share_url || share_url == "")
            share_url = window.location;

        if(by == "email") {
            url = "mailto:?subject=[What About The Flavour]*&body=" + share_url;
            window.open(url, '_self');
        }
        else if(by == "facebook") {
            url = "https://www.facebook.com/sharer/sharer.php?u=" + share_url;
            window.open(url, '_blank');
        }
        else if(by == "twitter") {
            url = "http://twitter.com/share?text=[What About The Flavour]*&url=" + share_url;
            window.open(url, '_blank');
        }
        // tracking
        this.trackSocial(by, "Partager", url);
    },

    /**
     * Track a page
     */
    trackPage: function(url, title) {

        // try with Path.routes
        if(Path.routes.current)
            url = Path.routes.current;
        if(Path.routes.currentTitle)
            title = Path.routes.currentTitle;

        // try with window.location
        if(!url)
            url = window.location.href;
        if(!title)
            title = document.title;

        if(this.log)
            console.debug('TrackPage( page: "' + url + '", title: "' + title + '" )');
        if(window.ga)
            ga('send', 'pageview', { 'page': url, 'title': title } );
    },

    /**
     * Track social sharing
     */
    trackSocial: function(network, action, target) {
        if(window.ga)
            ga('send', 'social', network, action, target);
    },

    /**
     * Track event
     */
    trackEvent: function (category, action, label, value) {
        if(window.ga)
            ga('send', 'event', category, action, label, value);
    },

    /**
     * Open or close the navigation menu
     */
    openCloseMenu: function() {
        // Open
        if(!this.menuOpen) {
            this.navContainer.style.display = "block";
            this.mainContainer.addEventListener('webkitTransitionEnd', onMenuOpenEnd, false);
            this.mainContainer.addEventListener('transitionend', onMenuOpenEnd, false);
            document.body.classList.add("menu-open");
        }
        // Close
        else {
            this.mainContainer.addEventListener('webkitTransitionEnd', onMenuCloseEnd, false);
            this.mainContainer.addEventListener('transitionend', onMenuCloseEnd, false);
            document.body.classList.remove("menu-open");
        }

        function onMenuOpenEnd(event) {
            Single.mainContainer.removeEventListener(event.type, arguments.callee, false);
            Single.menuOpen = true;
        }

        function onMenuCloseEnd(event) {
            Single.mainContainer.removeEventListener(event.type, arguments.callee, false);
            Single.menuOpen = false;
            Single.navContainer.style.display = "none";
        }
    },

    /**
     * Fill a HTML5 video on fullscreen
     */
    fillVideo: function(video) {
        if(!video || video.canPlayType == undefined)
            return;
        var videoRatio = video.videoWidth / video.videoHeight;
        var windowRatio = window.innerWidth / window.innerHeight;
        //console.debug("video: " + videoRatio + ", window: " + windowRatio);    
        // window is longer than video
        if(windowRatio > videoRatio) {
            var newHeight = window.innerWidth/videoRatio;
            video.style.height = newHeight+"px";
            video.style.width = "100%";
        }
        // window is taller than video
        else if(windowRatio < videoRatio) {
            var newWidth = window.innerHeight*videoRatio;
            video.style.width = newWidth+"px";
            video.style.height = "100%";
        }
        else {
            video.style.width = window.innerWidth+"px";
            video.style.height = window.innerWidth+"px";
        }

    },

    /*--------------------------------------
     Private functions
     ---------------------------------------*/

    _registerMetaTags: function() {

        // facebook open graph
        this.metaTags["og:type"] = document.querySelector('meta[property="og:type"]');
        this.metaTags["og:url"] = document.querySelector('meta[property="og:url"]');
        this.metaTags["og:title"] = document.querySelector('meta[property="og:title"]');
        this.metaTags["og:description"] = document.querySelector('meta[property="og:description"]');
        this.metaTags["og:image"] = document.querySelector('meta[property="og:image"]');
        // Twitter
        this.metaTags["twitter:url"] = document.querySelector('meta[name="twitter:url"]');
        this.metaTags["twitter:title"] = document.querySelector('meta[name="twitter:title"]');
        this.metaTags["twitter:description"] = document.querySelector('meta[name="twitter:description"]');
        this.metaTags["twitter:image"] = document.querySelector('meta[name="twitter:image"]');

        // save defaults - Take Facebook as master !
        this.metaTags["default:type"] = this.metaTags["og:type"].getAttribute("content");
        this.metaTags["default:url"] = this.metaTags["og:url"].getAttribute("content");
        this.metaTags["default:title"] = this.metaTags["og:title"].getAttribute("content");
        this.metaTags["default:description"] = this.metaTags["og:description"].getAttribute("content");
        this.metaTags["default:image"] = this.metaTags["og:image"].getAttribute("content");
    },

    /**
     * Add sections of the single page
     */
    _registerSections: function() {
        if(this.swiper.slides <= 0)
            return;

        var slide;
        this.sectionCount = this.swiper.slides.length;

        // register all sections
        for (var i = 0; i < this.sectionCount; i++) {

            slide = this.swiper.slides[i];
            if(slide.id == undefined || slide.id == "")
                slide.id = "section-" + i;

            // convert id in path
            var path = slide.id.replace(this.idSeparator, '/');

            // push path in slide.data
            slide.data('path', path);

            // register the index of section by path
            this.sectionIndexes[path] = i;

            if(this.log)
                console.debug( i +" : " + path);
        }

    },

    /**
     * Map all Path* paths
     * 3 levels :
     * --- • Section
     * ------ • Article
     * ------------ • Media
     */
    _registerPath: function() {

        // Map routes
        //Path.map(this.rootPath).to(this._routeBase);
        Path.map(this.rootPath + ":section(/:article)(/:mediatype/:media)").to( this._routeMap );

        // 404: route base
        Path.rescue(this._routeBase);

        // Listen Path*
        Path.history.listen(true);
    },

    /**
     * First route: /
     */
    _routeBase: function() {
        // Tracking        
        Single.trackPage();
    },

    /**
     * Others routes: /:section/:article/:mediatype/:media
     */
    _routeMap: function() {

        if(Single.log) {
            console.debug("= = = = = = = = = = = = = = =");
            console.debug(Path.routes.current);
        }

        // create metadata
        var metadata = {
            url: (window.location.origin + Path.routes.current),
            title: document.title
        };

        // close menu if open
        if(Single.menuOpen)
            Single.openCloseMenu();

        // Get first path
        var path = this.params.section;
        if(this.params.article && this.params.article != "media")
            path += '/' + this.params.article;

        // [Slide]*
        Single._slideByPath(path);

        // [Media]*
        if(this.params.mediatype && this.params.media) {

            // Open the media
            Single._openMedia(this.params.media, this.params.mediatype, path);

            // Get media title            
            var mediaLink = document.getElementById(this.params.media);

            if(mediaLink && mediaLink.title)
                metadata.title = mediaLink.title;

            metadata.type = "media";
        }
        // [Article]* or [Section]*
        else {
            // Stop Prevent pushState
            if(Single.preventPushState)
                Single.preventPushState = false;

            // Close media
            if(Single.mediaOpen)
                Single._closeMedia();

            var slide = Single.swiper.activeSlide();
            metadata.type = "article";

            metadata.title = slide.getAttribute('data-title');

            metadata.description = slide.getAttribute('data-description');

            var image = slide.querySelector(".article-visual > img");
            if(image)
                metadata.image = window.location.origin + Single.rootPath + image.getAttribute('src');
        }

        // Update Metadata
        Single._updateMetadata(metadata);

        // Track
        Single.trackPage();
    },

    /**
     * Slide by the path
     */
    _slideByPath: function(path) {
        // Get index
        var index = this.sectionIndexes[path];
        if(index < 0)
            index = 0;

        if(this.log)
            console.debug("Slide by path : " + path + " - index: " + index );

        // Slide to the slide
        this.swiper.swipeTo(index);
    },

    /**
     * Slide ended and update path URI
     */
    _slideChanged: function() {
        if(this.log)
            console.debug("Slide changed.");
        if(this.preventPushState) {
            this.preventPushState = false;
            return;
        }
        Single._pushCurrentSlide();
    },

    /**
     * Push the current slide
     */
    _pushCurrentSlide: function() {
        // Get the current slide        
        var slide = this.swiper.activeSlide();
        if(!slide)
            return;

        var newPath = this.rootPath + slide.data('path');
        var newTitle = slide.getAttribute('data-title');

        if(this.log)
            console.debug("PushState( path: "+newPath+", title: " + newTitle + " )");

        Path.history.pushState({title: newTitle, path: newPath}, newTitle, newPath);
    },

    _updateMetadata: function(metadata) {
        if(Single.log)
            console.debug(metadata);

        // set defaults if not value
        if(!metadata.type) metadata.type = this.metaTags["default:type"];
        if(!metadata.url) metadata.url = this.metaTags["default:url"];
        if(!metadata.title) metadata.title = this.metaTags["default:title"];
        if(!metadata.description) metadata.description = this.metaTags["default:description"];
        if(!metadata.image) metadata.image = this.metaTags["default:image"];

        // Set to Path and document
        Path.routes.currentTitle = metadata.title;
        document.title = metadata.title;

        // update meta tags
        this.metaTags["og:type"].setAttribute("content", metadata.type);
        this.metaTags["og:url"].setAttribute("content", metadata.url);
        this.metaTags["og:title"].setAttribute("content", metadata.title);
        this.metaTags["og:description"].setAttribute("content", metadata.description);
        this.metaTags["og:image"].setAttribute("content", metadata.image);

        this.metaTags["twitter:url"].setAttribute("content", metadata.url);
        this.metaTags["twitter:title"].setAttribute("content", metadata.title);
        this.metaTags["twitter:description"].setAttribute("content", metadata.description);
        this.metaTags["twitter:image"].setAttribute("content", metadata.image);

    },

    /**
     * Open a media
     * //www.youtube.com/embed/d3VU4F2zkpM?rel=0
     * //player.vimeo.com/video/18391916?byline=0&amp;portrait=0
     * //www.watf.fr/img/media.jpg
     */
    _openMedia: function(media, mediatype, article) {
        //console.debug("openMedia: " + media);

        if(!media)
            return;

        var isYoutube = (mediatype == "youtube") ? true : false;
        var isVimeo = (mediatype == "vimeo") ? true : false;
        var isNowness = (mediatype == "nowness") ? true : false;
        var isMedia = (mediatype == "media") ? true : false;
        var isDaily = (mediatype == "dailymotion") ? true : false;

        // set as current
        this.currentMedia = media;
        this.currentMediaType = mediatype;

        var player;

        if(isYoutube) {
            player = document.getElementById("youtube-player");
            player.setAttribute("src", "//www.youtube.com/embed/" + this.currentMedia + "?rel=0");
            this.mediaContainer.classList.remove('vimeo');
            this.mediaContainer.classList.remove('media');
            this.mediaContainer.classList.add('youtube');
        }
        else if(isDaily) {
            player = document.getElementById("youtube-player");
            player.setAttribute("src", "//www.dailymotion.com/embed/video/" + this.currentMedia);
            this.mediaContainer.classList.remove('vimeo');
            this.mediaContainer.classList.remove('media');
            this.mediaContainer.classList.add('youtube');
        }
        else if(isVimeo) {
            player = document.getElementById("vimeo-player");
            player.setAttribute("src", "//player.vimeo.com/video/" + this.currentMedia + "?byline=0&amp;portrait=0");
            this.mediaContainer.classList.remove('youtube');
            this.mediaContainer.classList.remove('media');
            this.mediaContainer.classList.add('vimeo');
        }
        else if(isNowness) {
            var param = this.currentMedia.split('-');
            player = document.getElementById("vimeo-player");
            player.setAttribute("src", "//www.nowness.com/media/embedvideo?itemid="+param[0]+"&issueid="+param[1]);
            this.mediaContainer.classList.remove('youtube');
            this.mediaContainer.classList.remove('media');
            this.mediaContainer.classList.add('vimeo');
        }
        else if(isMedia) {
            player = document.getElementById("media-viewer");
            player.setAttribute("src", this.rootPath + '/media/' + this.currentMedia + '.jpg');
            this.mediaContainer.classList.remove('vimeo');
            this.mediaContainer.classList.remove('youtube');
            this.mediaContainer.classList.add('media');
        }

        // activate close button
        var closeBtn = document.getElementById('media-close-btn');
        closeBtn.addEventListener('click', close);

        // show the media
        this.mediaOpen = true;
        this.mediaContainer.classList.add('enabled');

        var self = this;

        function close(event) {
            self.mediaContainer.classList.remove('enabled');
            closeBtn.removeEventListener(event.type, arguments.callee, false);
            player.setAttribute("src", "");
            self.mediaOpen = false;
            Single._pushCurrentSlide();
        }
    },

    /**
     * Close a media
     */
    _closeMedia:function() {
        this.mediaContainer.classList.remove('enabled');
        this.currentMedia = null;
    },

    /**
     * Active all internal links with History API (wrapped by Path)
     */
    _activeLinks: function() {
        // Change all links
        var links = document.getElementsByTagName("a");
        for (var i = 0; i < links.length; i++) {
            if(links[i].target != "_blank" && links[i].target != "_self" && links[i].pathname) {
                links[i].onclick = function(e) {
                    e.preventDefault();
                    //console.debug("[Link]* PushState( path: "+e.currentTarget.pathname+", title: " + e.currentTarget.title + " )");
                    Single.preventPushState = true;
                    Path.history.pushState({title: e.currentTarget.title, path: e.currentTarget.pathname}, e.currentTarget.title, e.currentTarget.pathname);
                }
            }
        }
    },


}