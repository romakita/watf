function include(fileName) {
	document.write("<script type='text/javascript' src='"+fileName+"' charset='utf-8'></script>" );
}
include("/js/jquery.min.js");
include("/js/idangerous.swiper-2.6.min.js");
include("/js/path.js");
include("/js/single.js");

document.addEventListener("DOMContentLoaded", function(event) {
    
    document.removeEventListener(event.type, arguments.callee, false);
    console.log("DOMContentLoaded.");

    // Init Swiper*
    var swiper = new Swiper('.swiper-container', {
        mode: 'vertical',
        speed: 400,
        keyboardControl: true,
        pagination: '.pagination',
        paginationClickable: true,
        simulateTouch: false,
        onSlideChangeEnd: onSlideChangeEnd
    });

    // Init Single*
    Single.init({
        log: false,
        lang: "fr",
        swiper: swiper,
        idSepartator: "__",
        mainContainer: document.getElementById("main-container"),
        mediaContainer: document.getElementById("media-container"),
        onComplete: function(params) {
            console.debug("Welcome to the [WATF]*");
            window.onscroll = onScrolling;
        }
    });  
    
});

function onSlideChangeEnd(swiper) {
    window.onscroll = onScrolling;
    Single.onSlideChangeEnd.call(this, swiper);
}

function onScrolling(event) {
    console.debug("scroll: " + document.body.scrollTop);
    if(document.body.scrollTop > 0) {
        if(Single.gotoNext())
            window.onscroll = null;
    }
    else if(document.body.scrollTop < 0) {
        if(Single.gotoPrevious())
            window.onscroll = null;
    }

} 
